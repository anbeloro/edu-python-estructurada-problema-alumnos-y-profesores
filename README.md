# README #

### Para qué es este repo? ###

Este repo plantea un ejercicio sencillo para quien se inicia en la programación.

Sin conocimientos sobre Programación Orienta a Objetos, pretende desde un enfoque de programación estructurada, implementar un programa interactivo por shell.

Una demo del programa:

![Alt text](https://bytebucket.org/agrasar/edu-python-estructurada-problema-alumnos-y-profesores/raw/9e75d1582317ae8aaa08ac4cc2ae8f35e6eecd43/doc/JavaEstructuradoAlumnos.gif)

### Lo que debe el estudiante de programación implementar ###
El programa debe mostrar un menú con 5 opciones nada más ejecutarlo, como se muestra en el GIF de la demo

1. Debe mostrar una lista precargada de alumnos. La lista la generas cuando arranques el programa. Puedes utilizar los alumnos que comparten clase actualmente contigo. Después la imprimes por consola tal y como sale en la demo.

2. La segunda opción permitirá, una vez el programa esté siendo ejecutado, añadir un nuevo alumno a la lista. Pedirá nombre y apellido del alumno (esta información no debe persistir una vez finalice el programa).

3. La tercer opción generará una nota aleatorio de un examen para cada alumno. Una vez presionada esta opción debe aparecer un submenu que me permita seleccionar la dificultad del examen (Alta, Media, Baja). En función de la selección la nota aleatoria se generará de la siguiente manera:
	* Baja: Un número decimal aleatorio entre 5 y 10.
	* Media: Un número decimal aleatorio entre 0 y 10.
* Alta: Dos números decimales aleatorios entre 0 y 10. La nota resultante será el mínimo de estos dos números aleatorios generados.
4. La cuarta opción imprimirá el boletín de notas de todos los alumnos tal y como se muestra en la demo. Ten en cuenta que puede haber alumnos que no hayan realizado todos los exámenes
5. La última opción permitirá salir del programa mostrando un mensaje de "Bye, vuelve pronto!" 

### Lo que se pretende aprender de este ejercicio ###

* Declaración de variables. Tipo y ámbito.
* Uso de funciones. Principio de Reutilización del Código
* Programación estructurada
* Librerías
* Estructuras básicas de datos
* Interacción por shell
* Validación y correctitud de datos de entrada
* Estructuras básicas de control
* Bucles
* Entrada y salida de datos
* Operadores Aritmético-lógicos

### Antes de ponerte a trabajar... ###

##### Haz un fork del repositorio original ####
Haz un fork del repositorio original y configúralo de forma privada (la actividad propuesta es individual ;)
Habilita las issues e indica que es un proyecto Python.

##### Clona el repositorio ####
```
git clone https://agrasar@bitbucket.org/agrasar/edu-python-estructurada-problema-alumnos-y-profesores.git
```

##### Ejecuta el programa ####
Antes de ponerte a trabajar, asegúrate que puedes ejecutar el programa. De esta forma, comprobarás que tienes el intérprete instalado en su versión correcta.

```
python3 programa.py
```

##### Crea tu rama de trabajo ####
Crea tu propia rama de trabajo! Crea una nueva rama a partir de master que se llame como el nombre de tu usuario en el curso. Te recuerdo cómo:

```
git checkout -b <usuario>
```
Tu solución final deberá estar apuntada por esta rama. Puedes utilizar todas las ramas que quieras, pero no trabajes en la master y asegúrate, si tienes otras ramas que forman parte de tu solución, de combinarlas con tu rama con el nombre de tu usuario.

### Cuándo termines tu trabajo... ###

##### Etiqueta tu versión ####

Cuando tengas un revisión de tu código que consideres estable, etiquétala de la siguiente manera
v1.y.z-usuario

* Donde y, es un número que incrementarás empezando por el 0, cuando realices cambios en tu programa que se puedan apreciar desde la interfaz gráfica
* Donde z, es un número que incrementarás empezando por el 0, cuando realices correcciones en tu programa o refactorizaciones.

Puedes hacer etiquetado de tu último commit de la siguiente manera:

```
# Si quieres hacer una etiqueta ligera (solo nombrar un commit
git tag <etiqueta>

# Si quieres hacer una etiqueta que contenga más información
git tag -a <etiqueta> -m 'El mensaje'
```

Si quieres poner una etiqueta a un commit anterior, pon su checksum al final de las instrucciones anteriores.

Recuerda enviar tus tags a tus repos remotos de la siguiente manera:

```
git push <remoto> <tag>
```

Consulta esta [fuente](https://git-scm.com/book/es/v2/Fundamentos-de-Git-Etiquetado) para más detalles.

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact
